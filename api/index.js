const config = require('dotenv').config()
const express = require('express')
const defaultRoutes = require('./routes/index')
const paymentRoutes = require('./routes/payment')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

const apiDocumentationURl = 'https://documenter.getpostman.com/view/1667172/S1EQUyEY#4ce95043-b988-430e-b9af-9a17bd5c30a7'

console.log(process.env.MONGO_HOST , config)
const app = express()
app.use(bodyParser.urlencoded({
  extended: true,
}))
app.use(bodyParser.json())

app.use(function (error, req, res, next) {
  if (error instanceof SyntaxError) {
    res.status(400).json(error)
    return
  } else {
    next()
  }
})

app.all('/api/*', function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, GET, DELETE, OPTIONS')
  next()
})


mongoose.connect(`mongodb://${process.env.MONGO_HOST}/data-table`, { useNewUrlParser: true })

var port = process.env.PORT || 3000

app.get('/', (req, res) => res.send(`Please find docs reference at ${apiDocumentationURl}`))

app.use('/api', defaultRoutes)
app.use('/api', paymentRoutes)

app.listen(port, function () {
  console.info(`Running - DataTable API is available on port ${port}`)  // eslint-disable-line no-console
})
