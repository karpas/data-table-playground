const Payment = require('../models/payment')
const { validationResult } = require('express-validator/check')

exports.index = function (req, res) {
  Payment.get(function (err, payments) {
    if (err) {
      res.json({
        status: 'error',
        message: err,
      })
      return
    }
    res.json({
      status: 'success',
      message: 'Payments retrieved successfully',
      data: payments,
    })
  })
}

exports.new = function (req, res) {
  const payment = new Payment()
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() })
  }

  payment.id = req.body.id
  payment.name = req.body.name
  payment.description = req.body.description
  payment.date = req.body.date
  payment.amount = req.body.amount

  payment.save(function (err) {
    if (err) {
      res.send(err)
      return
    }
    res.json({
      message: 'New payment created!',
      data: payment,
    })
  })
}

exports.view = function (req, res) {
  Payment.findById(req.params.id, function (err, payment) {
    if (err) {
      res.send(err)
      return
    }

    res.json({
      message: 'Payment details',
      data: payment,
    })
  })
}

exports.update = function (req, res) {
  Payment.findById(req.params.id, function (err, payment) {
    if (err) {
      res.send(err)
      return
    }

    payment.id = req.body.id ? req.body.id : payment.id
    payment.name = req.body.name ? req.body.name : payment.name
    payment.description = req.body.description ? req.body.description : payment.description
    payment.date = req.body.date ? req.body.date : payment.date
    payment.amount = req.body.amount ? req.body.amount : payment.amount

    payment.save(function (err) {
      if (err) {
        res.send(err)
        return
      }
      res.json({
        message: 'Payment details updated',
        data: payment,
      })
    })
  })
}

exports.delete = function (req, res) {
  Payment.remove({
    _id: req.params.id,
  }, function (err) {
    if (err) {
      res.send(err)
      return
    }
    res.json({
      status: 'Successfully deleted payment',
    })
  })
}
