const router = require('express').Router()

router.get('/', function (req, res) {
  res.json({
    status: 'API Is Working properly',
    message: 'Welcome to REST API for data-table',
  })
})

module.exports = router
