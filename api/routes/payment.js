const router = require('express').Router()
const paymentController = require('../controllers/payment')
const { check } = require('express-validator/check')


router.route('/payment')
  .get(paymentController.index)
  .post([
    check('id').exists(),
    check('name').exists(),
    check('description').exists(),
    check('date').exists(),
    check('amount').exists(),
  ], paymentController.new)

router.route('/payment/:id')
  .get(paymentController.view)
  .patch(paymentController.update)
  .put([
    check('id').exists(),
    check('name').exists(),
    check('description').exists(),
    check('date').exists(),
    check('amount').exists(),
  ], paymentController.update)
  .delete(paymentController.delete)

module.exports = router
