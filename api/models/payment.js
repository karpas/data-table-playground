const mongoose = require('mongoose')

const paymentSchema = mongoose.Schema({
  id: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  date: {
    type: Date,
    required: true,
  },
  amount: {
    type: Number,
    required: true,
  },
})

const Payment = module.exports = mongoose.model('payment', paymentSchema)
module.exports.get = function (callback, limit = 100) {
  Payment.find(callback).limit(limit)
}
