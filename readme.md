# Project Title

Example project created to play with vue, express, node and docker.

## Getting Started

### Prerequisites

Things you need to install

```
Docker
node.js
```

### Installing

You can get a development env by running


```
docker-compose up
```

## Running the tests

Few examples are provided for front-end applicatnion.

```
cd app
yarn test:unit
```

## Cover Letter part

### Time

Spent around 8 hours divided into creating from scratch backend and frontend with little help of Vue-cli.

[Toggl summary report](./Toggl_summary_report.pdf)

### Component adaptation

* expose more detailed interface to make it more customizable
* allow custom filters for cells for data formatting (eg. dates)
* wrap elements with more narrow css classes to allow easy visual customisation

### Recent CSS Property

Maybe not recent but I'm in love with flexbox. Making super easy dealing with different screen sizes and content arrangement.

### Modern JS Feature

Template literals - I hate stitching strings :)

### Third-party Vue.js library

I don't have favourite, but I play with Vuetify recently a lot and I like it a lot.
