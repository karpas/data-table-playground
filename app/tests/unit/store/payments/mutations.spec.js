import { expect } from "chai";
import paymentsModule from "../../../../src/store/payments";

// destructure assign `mutations`
const SET_FETCHING_FLAG = "Set payments fetching flag";
const SET_PAYMENTS = "Set payments list";

describe("Payment - mutations", () => {
  it(SET_FETCHING_FLAG, () => {
    const state = { fetching: false };
    paymentsModule.mutations[SET_FETCHING_FLAG](state, true);
    expect(state.fetching).to.equal(true);
  });

  it(SET_PAYMENTS, () => {
    const state = { all: [] };
    const payments = [{ id: 1 }];
    paymentsModule.mutations[SET_PAYMENTS](state, payments);
    expect(state.all).to.equal(payments);
  });
});
