import { fetchAllPayments, patchPayment } from "@/api/payment.service";

const SET_FETCHING_FLAG = "Set payments fetching flag";
const SET_PAYMENTS = "Set payments list";
const UPDATE_PAYMENT = "Update payment";

export default {
  state: {
    fetching: false,
    all: []
  },
  mutations: {
    [SET_FETCHING_FLAG](state, status) {
      state.fetching = status;
    },
    [SET_PAYMENTS](state, payments) {
      state.all = payments;
    },
    [UPDATE_PAYMENT](state, { _id, editCell }) {
      this._vm._.find(state.all, payment => payment._id == _id)[editCell.key] =
        editCell.data;
    }
  },
  actions: {
    async getAllPayments({ commit }) {
      commit(SET_FETCHING_FLAG, true);
      const result = await fetchAllPayments();

      commit(SET_PAYMENTS, result.data.data);

      commit(SET_FETCHING_FLAG, false);
    },

    async updatePayment({ commit }, { _id, editCell }) {
      let patch = {}
      patch[editCell.key] = editCell.data;
      const result = await patchPayment({
        _id,
        patch
      });

      if (result.status === 200) {
        commit(UPDATE_PAYMENT, { _id, editCell });
      }
    }
  }
};
