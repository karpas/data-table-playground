import axios from "axios";
const routes = {
  payment: "/payment"
};

const getUrl = (endpoint, params) => {
  let url = `http://${process.env.VUE_APP_API_HOST}/api${routes[endpoint]}`;

  if (params) {
    url += `/${params}`;
  }

  return url;
};

export default {
  getUrl,
  axios
};
