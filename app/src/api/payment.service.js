import client from "./client";

export const fetchAllPayments = () => {
  return client.axios.get(client.getUrl("payment"));
};

export const patchPayment = data => {
  return client.axios.patch(client.getUrl("payment", data._id), data.patch);
};
