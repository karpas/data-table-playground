import Vue from "vue";
import App from "./App.vue";
import store from "./store/store";
import VueLodash from "vue-lodash";

Vue.config.productionTip = false;

Vue.use(VueLodash);

new Vue({
  store,
  render: h => h(App)
}).$mount("#app");
