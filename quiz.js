// Assuming you have already done "npm install fernet"
let fernet = require('fernet')
let secret = new fernet.Secret('TluxwB3fV_GWuLkR1_BzGs1Zk90TYAuhNMZP_0q4WyM=')
// Oh no! The code is going over the edge! What are you going to do?
let message = 'gAAAAABcsZ4hjDtZGypuEWDKOn5AVgc6wWqbhzPVeZZ_llcN2fBZdgLW7O3FgqLSqRdwdBor5mB8LCPXDHMS624ssIjm7rOk7QlvJ74FKPdiqkLdW48vD71PtThfPjO0ReeJHfwrU4M887NtF9ECfAR9EfqF74Osv7hnRuzVsr5gI2kXsvneKQ3WjDM6ztQcvk8Nces1s9nW'


let token = new fernet.Token({ secret, token: message, ttl:0})
console.log(token.decode())
